# frozen_string_literal: true

module DesktopEntryModule
  # Represents the .desktop entry data
  class DesktopEntry
    attr_reader :name, :comment, :exec, :icon, :categories

    def initialize
      puts 'Enter a name for the desktop entry:'
      @name = gets.chop

      puts 'Enter a comment for the desktop entry:'
      @comment = gets.chop

      puts 'Enter a command line (exec) for the desktop entry:'
      @exec = gets.chop

      puts 'Enter a categories for the desktop entry:'
      @categories = gets.chop
    end
  end
end
