require 'erb'

class EntryTemplate
  def self.fill_template(entry, path)
    template = ERB.new <<~TEMPLATE_END
      [Desktop Entry]
      Name=<%= entry.name %>
      Comment=<%= entry.comment %>
      Exec=<%= entry.exec %>
      Terminal=false
      Type=Application
      Categories=<%= entry.categories %>;
    TEMPLATE_END

    write_template(template.result(binding), path)
  end

  def self.write_template(template, path)
    File.open(path, 'w') { |file| file.write(template) }
    puts "Template saved here: #{path}"
  end
end
