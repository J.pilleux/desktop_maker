require 'optparse'

# The option parser for the application
class OptParser
  DEFAULT_LOCATION = "#{Dir.home}/.local/share/applications".freeze
  @options = {}
  @options[:path] = DEFAULT_LOCATION

  def self.parse(args)
    opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: desktop_maker -n NAME [options]'

      opts.separator ''
      opts.separator 'Mandatory arguments'

      opts.on('-n', '--name NAME', 'The desktop entry file name') do |name|
        @options[:name] = name
      end

      opts.separator ''
      opts.separator 'Optional arguments'

      opts.on('-p', '--path PATH', "Desktop entry path, default value: #{DEFAULT_LOCATION}") do |path|
        @options[:path] = path
      end

      opts.on_tail('-h', '--help', 'Show this message') do
        puts opts
        exit
      end
    end

    opt_parser.parse!(args)
    abort(opt_parser.help) if @options[:name].nil?
    abort("Aborting: #{@options[:path]} does not exists") unless File.directory?(@options[:path])

    desktop_file = "#{@options[:path]}/#{@options[:name]}.desktop"
    abort("Aborting: #{desktop_file} already exists") if File.exist?(desktop_file)

    @options[:desktop_path] = desktop_file
    @options
  end
end
